# nix-flake-wuffs-c

A nix flake to build wuffs-c


## Use

Prerequisite: You'll need a flake-enabled nix system; see https://nixos.wiki/wiki/Flakes for details.

To run: TODO put a real command here
```
nix run gitlab:yjftsjthsd-g/nix-flake-wuffs-c
```


## License

This repo - a bunch of build scripts, basically - is released under the MIT
license, but wuffs itself is under the Apache 2.0 license.

