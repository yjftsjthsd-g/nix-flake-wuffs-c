{ pkgs ? import <nixpkgs> { } }:

with pkgs;

buildGoModule rec {
  pname = "wuffs-c";
  version = "2f65d6";

  nativeBuildInputs = [ pkg-config ];
  buildInputs = [ pkgs.zstd pkgs.lz4 pkgs.zlib ];

  # https://github.com/google/wuffs/tree/main/cmd/wuffs-c
  src = fetchFromGitHub {
    owner = "google";
    repo = "wuffs";
    #rev = "v${version}";
    rev = "2f65d6fc74686f4906c4523e1d5630f1920dc595";
    hash = "sha256-ApHUI1o7JvAD0JDtFSgt6sCvCFngPfgnf590MHPVv3Q=";
  };

  #vendorHash = "";
  vendorSha256 = "sha256-rHsCizC4JMM2pvALTjslpjmokcx9foytoPGPwH9Rm80=";

  meta = with lib; {
    description = "wuffs build tools";
    homepage = "https://github.com/google/wuffs";
    license = licenses.asl20;
    maintainers = [ "yjftsjthsd-g" ];
  };
}
